# Recordings-MVVM-C

Sample App copied from [Objc.io app-architecture examples](https://github.com/objcio/app-architecture).

# Local Bitrise testing

    # Run bitrise by downloading the binaries from the internet
    $ bitrise run analyze

    # Run the binaries from disk. Beware that both ast-swift and bm-runner
    # should have been built in debug before.
    $ SANDBOX=1 bitrise run analyze

# Built with Carthage

    $ carthage update --platform iOS